<?php
// 一些杂项

namespace DiscuzDb\db\concern;

use PDO;

trait Other
{
  /**
   * 根据参数绑定组装最终的SQL语句 便于调试
   *
   * @access public
   * @param string $sql  带参数绑定的sql语句
   * @param array  $bind 参数绑定列表
   * @return string
   */
  public function getRealSql(string $sql, array $bind = []): string
  {
    foreach ($bind as $key => $val) {
      $value = strval(is_array($val) ? $val[0] : $val);
      $type  = is_array($val) ? $val[1] : PDO::PARAM_STR;
      
      if (21 == $type || PDO::PARAM_STR == $type) {
        $value = '\'' . addcslashes($value, "'") . '\'';
      } elseif (PDO::PARAM_INT == $type && '' === $value) {
        $value = '0';
      }
      
      // 判断占位符
      $sql = is_numeric($key) ?
        substr_replace($sql, $value, strpos($sql, '?'), 1) :
        substr_replace($sql, $value, strpos($sql, ':' . $key), strlen(':' . $key));
    }
    
    return rtrim($sql);
  }
}
