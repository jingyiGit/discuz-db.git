<?php

namespace DiscuzDb;

use DiscuzDb\Facade;
use DiscuzDb\db\DbManager;


/**
 * @see \DiscuzDb\db\DbManager
 * @mixin \DiscuzDb\db\DbManager
 */
class discuz_extra extends Facade
{
  protected static $pk = 'id';
  protected static $table = '';
  protected static $prefix = 'pre_';
  protected static $schema = [];
  
  public static function db($getSql = false)
  {
    $config = [
      'pk'     => static::$pk,
      'table'  => static::$table,
      'prefix' => static::$prefix,
      'schema' => static::$schema,
      'getSql' => $getSql,
    ];
    return new DbManager($config);
  }
  
  public static function __callStatic($method, $params)
  {
    return static::db()->$method(...$params);
  }
  
  /**
   * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
   *
   * @access protected
   * @return string
   */
  protected static function getFacadeClass()
  {
    return 'DiscuzDb\db\DbManager';
  }
}

